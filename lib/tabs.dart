import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'screens/feed.dart';
import 'screens/activity.dart';
import 'screens/workout.dart';
import 'screens/inbox.dart';
import 'screens/screen5.dart';

// Main code for all the tabs
class MyTabs extends StatefulWidget {
  @override
  MyTabsState createState() => new MyTabsState();
}

class MyTabsState extends State<MyTabs> with SingleTickerProviderStateMixin {
  static final homePageKey = GlobalKey<MyTabsState>();
  TabController tabcontroller;

  @override
  void initState() {
    super.initState();
    tabcontroller = new TabController(vsync: this, length: 4);
  }

  @override
  void dispose() {
    super.dispose();
    tabcontroller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: homePageKey,
      appBar: new AppBar(
        centerTitle: true,
        title: new Text("Feed"),
        leading: new Padding(
          padding: EdgeInsets.all(7.0),
          child: new Container(
            height: 10,
            width: 10,
              decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                  image: new DecorationImage(
                      fit: BoxFit.fill,
                      image: new NetworkImage(
                          "https://i.imgur.com/BoN9kdC.png")
                  )
              ),
          ),
        ),
      ),
      bottomNavigationBar: new Material(
          child: new TabBar(
              controller: tabcontroller,
              unselectedLabelColor: Colors.grey,
              labelColor: Colors.grey[900],
              labelStyle: TextStyle(fontSize: 11.0),
              tabs: <Tab>[
                new Tab(
                  //icon: new Icon(Icons.chat),
                  //icon: new Icon(Icons.chrome_reader_mode),
                  icon: new Icon(Icons.featured_play_list),
                ),
                new Tab(
                  icon: new Icon(Icons.equalizer),
                ),
                new Tab(
                  icon: new Icon(Icons.alarm),
                ),
                new Tab(
                  icon: new Icon(Icons.email),
                ),
                /*
                new Tab(
                  icon: new Icon(Icons.folder),
                  text: "Library",
                ),
                */
              ])),
      body: new TabBarView(controller: tabcontroller, children: <Widget>[
        // All the Class goes here
        Feed(),
        Activity(),
        Workouts(),
        Inbox(),
        //Library()
      ]),
    );
  }
}