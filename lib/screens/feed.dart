import 'package:flutter/material.dart';

class Feed extends StatefulWidget {
  Feed({Key key}) : super(key: key);

  _FeedState createState() => _FeedState();
}

class _FeedState extends State<Feed> {

  @override
  Widget build(BuildContext context) {
    return ListView(
      // This next line does the trick.
      scrollDirection: Axis.vertical,
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height * 0.82,
          child: new Stack(
            fit: StackFit.expand,
            children: <Widget>[
              new ClipRRect(
                borderRadius: new BorderRadius.circular(0.0),
                child: new Image.asset('assets/images/image1.JPG',
                  fit: BoxFit.cover,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30, left: 20, right: 20),
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  //mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: new Text('Nike Pegasus 36 Trail'.toUpperCase(),
                        style: new TextStyle(
                          color: Colors.white,
                          fontSize: 12.0,
                          fontWeight: FontWeight.bold,
                          //fontFamily: "",
                          letterSpacing: 1.0,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    new Text('Der Optimale Offroad-Schuh.'.toUpperCase(),
                      style: new TextStyle(
                        color: Colors.white,
                        fontSize: 30.0,
                        //fontFamily: "",
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1.0,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 30, left: 20, right: 20),
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  //mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new InkWell(
                      onTap: () => print('hello'),
                      child: new Container(
                        width: 100.0,
                        height: 40.0,
                        decoration: new BoxDecoration(
                          color: Colors.transparent,
                          border: new Border.all(
                              color: Colors.white, width: 1.0),
                          borderRadius: new BorderRadius.circular(5.0),
                        ),
                        child: new Center(child:
                        new Text('Kaufen'.toUpperCase(),
                          style: new TextStyle(
                              fontSize: 18.0,
                              color: Colors.white,
                              letterSpacing: 0.1,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          height: 300.0,
          color: Colors.white,
          child: Padding(
              padding: const EdgeInsets.only(top: 20),
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text("Mehr Freunde. Mehr Motivation.".toUpperCase(),
                    style: new TextStyle(
                      fontWeight: FontWeight.bold,
                      letterSpacing: 0.1,
                      fontSize: 20,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10, right: MediaQuery.of(context).size.width * 0.2, left: MediaQuery.of(context).size.width * 0.2),
                    child: Text("Füge Freunde hinzu, um die Nike Training Aktivität der anderen zu sehen, dich auf Bestenlisten zu messen und deine Statistiken zu zeigen",
                      style: new TextStyle(
                        fontSize: 11,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20, right: MediaQuery.of(context).size.width * 0.2, left: MediaQuery.of(context).size.width * 0.2),
                    child: new InkWell(
                      onTap: () => print('hello'),
                      child: new Container(
                        width: MediaQuery.of(context).size.width * 0.6,
                        height: 40.0,
                        decoration: new BoxDecoration(
                          color: Colors.transparent,
                          border: new Border.all(
                              color: Colors.grey, width: 1.0),
                        ),
                        child: new Center(child:
                        new Text('Freunde suchen'.toUpperCase(),
                          style: new TextStyle(
                              fontSize: 18.0,
                              color: Colors.black,
                              letterSpacing: 0.1,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.82,
          child: new Stack(
            fit: StackFit.expand,
            children: <Widget>[
              new ClipRRect(
                borderRadius: new BorderRadius.circular(0.0),
                child: new Image.asset('assets/images/image1.JPG',
                  fit: BoxFit.cover,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30, left: 20, right: 20),
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  //mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: new Text('Workout der Woche'.toUpperCase(),
                        style: new TextStyle(
                          color: Colors.white,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                          //fontFamily: "",
                          letterSpacing: 0.1,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: new Text('Tank top Arms'.toUpperCase(),
                        style: new TextStyle(
                          color: Colors.white,
                          fontSize: 30.0,
                          //fontFamily: "",
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.1,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 10, right: MediaQuery.of(context).size.width * 0.1, left: MediaQuery.of(context).size.width * 0.1),
                      child: new Text('Definiere deinen Körper mit diesem 20-minütigen Hantel-Workout für Schultern, Bizeps und Trizeps.',
                        style: new TextStyle(
                          color: Colors.white,
                          fontSize: 12.0,
                          //fontFamily: "",
                          fontWeight: FontWeight.normal,
                          letterSpacing: 0.1,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 50, left: 20, right: 20),
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  //mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new InkWell(
                      onTap: () => print('hello'),
                      child: new Container(
                        width: 150.0,
                        height: 40.0,
                        decoration: new BoxDecoration(
                          color: Colors.transparent,
                          border: new Border.all(
                              color: Colors.white, width: 1.0),
                          borderRadius: new BorderRadius.circular(5.0),
                        ),
                        child: new Center(child:
                          new Text('Weitere Infos'.toUpperCase(),
                            style: new TextStyle(
                                fontSize: 15.0,
                                color: Colors.white,
                                letterSpacing: 0.1,
                                fontWeight: FontWeight.bold
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  //mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        new FlatButton.icon(
                          color: Colors.white,
                          icon: Icon(Icons.favorite,
                          color: Colors.white),
                          label: Text("476", style: new TextStyle(
                            color: Colors.white,
                          ),
                          ),
                          onPressed: null,
                        ),
                        new FlatButton.icon(
                          color: Colors.white,
                          icon: Icon(Icons.mode_comment,
                              color: Colors.white),
                          label: Text("27", style: new TextStyle(
                            color: Colors.white,
                          ),
                          ),
                          onPressed: null,
                        ),
                        new FlatButton.icon(
                          color: Colors.white,
                          icon: Icon(Icons.redo,
                              color: Colors.white),
                          label: Text("", style: new TextStyle(
                            color: Colors.white,
                          ),
                          ),
                          onPressed: null,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.82,
          margin: EdgeInsets.only(top: 4),
          child: new Stack(
            fit: StackFit.expand,
            children: <Widget>[
              new ClipRRect(
                borderRadius: new BorderRadius.circular(0.0),
                child: new Image.asset('assets/images/image1.JPG',
                  fit: BoxFit.cover,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30, left: 20, right: 20),
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  //mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: new Text('Crossfit games 2019'.toUpperCase(),
                        style: new TextStyle(
                          color: Colors.white,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                          //fontFamily: "",
                          letterSpacing: 0.1,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: new Text('Willkommen in Mat-ison, Wisconsin'.toUpperCase(),
                        style: new TextStyle(
                          color: Colors.white,
                          fontSize: 30.0,
                          //fontFamily: "",
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.1,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 50, left: 20, right: 20),
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  //mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new InkWell(
                      onTap: () => print('hello'),
                      child: new Container(
                        width: 150.0,
                        height: 40.0,
                        decoration: new BoxDecoration(
                          color: Colors.transparent,
                          border: new Border.all(
                              color: Colors.white, width: 1.0),
                          borderRadius: new BorderRadius.circular(5.0),
                        ),
                        child: new Center(child:
                        new Text('Weitere Infos'.toUpperCase(),
                          style: new TextStyle(
                              fontSize: 15.0,
                              color: Colors.white,
                              letterSpacing: 0.1,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  //mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        new FlatButton.icon(
                          color: Colors.white,
                          icon: Icon(Icons.favorite,
                              color: Colors.white),
                          label: Text("86", style: new TextStyle(
                            color: Colors.white,
                          ),
                          ),
                          onPressed: null,
                        ),
                        new FlatButton.icon(
                          color: Colors.white,
                          icon: Icon(Icons.mode_comment,
                              color: Colors.white),
                          label: Text("3", style: new TextStyle(
                            color: Colors.white,
                          ),
                          ),
                          onPressed: null,
                        ),
                        new FlatButton.icon(
                          color: Colors.white,
                          icon: Icon(Icons.redo,
                              color: Colors.white),
                          label: Text("", style: new TextStyle(
                            color: Colors.white,
                          ),
                          ),
                          onPressed: null,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.82,
          child: new Stack(
            fit: StackFit.expand,
            children: <Widget>[
              new ClipRRect(
                borderRadius: new BorderRadius.circular(0.0),
                child: new Image.asset('assets/images/image1.JPG',
                  fit: BoxFit.cover,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30, left: 20, right: 20),
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  //mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: new Text('Lookbook'.toUpperCase(),
                        style: new TextStyle(
                          color: Colors.white,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                          //fontFamily: "",
                          letterSpacing: 0.1,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: new Text('Spüre den Flow'.toUpperCase(),
                        style: new TextStyle(
                          color: Colors.white,
                          fontSize: 30.0,
                          //fontFamily: "",
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.1,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 50, left: 20, right: 20),
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  //mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new InkWell(
                      onTap: () => print('hello'),
                      child: new Container(
                        width: 150.0,
                        height: 40.0,
                        decoration: new BoxDecoration(
                          color: Colors.transparent,
                          border: new Border.all(
                              color: Colors.white, width: 1.0),
                          borderRadius: new BorderRadius.circular(5.0),
                        ),
                        child: new Center(child:
                        new Text('Mehr Erfahren'.toUpperCase(),
                          style: new TextStyle(
                              fontSize: 15.0,
                              color: Colors.white,
                              letterSpacing: 0.1,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        new FlatButton.icon(
                          color: Colors.white,
                          icon: Icon(Icons.favorite,
                              color: Colors.white),
                          label: Text("24", style: new TextStyle(
                            color: Colors.white,
                          ),
                          ),
                          onPressed: null,
                        ),
                        new FlatButton.icon(
                          color: Colors.white,
                          icon: Icon(Icons.mode_comment,
                              color: Colors.white),
                          label: Text("", style: new TextStyle(
                            color: Colors.white,
                          ),
                          ),
                          onPressed: null,
                        ),
                        new FlatButton.icon(
                          color: Colors.white,
                          icon: Icon(Icons.redo,
                              color: Colors.white),
                          label: Text("", style: new TextStyle(
                            color: Colors.white,
                          ),
                          ),
                          onPressed: null,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}